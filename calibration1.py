import pandas as pd
import numpy as np

# This file calculates the covariance matrix of asset returns when denominated in USD
# Two methods are used:
# Method 1. Reciprocate fx so it's denominated in USD, and then applies fx rates so that equities are also denominated in USD.
# Method 2. Apply transformations to the covariance matrix which are equivalent to reciprocating and applying fx.

close_prices = pd.read_csv("close prices.csv", index_col=0, parse_dates=True, dayfirst=True).dropna()
instruments = pd.read_csv("instruments.csv", index_col=0)
market_portfolio_weights = instruments['Weight']

#### METHOD 1 - BRUTE FORCE

close_prices_in_usd = close_prices.copy()
close_prices_in_usd['CAD='] = 1.0 / close_prices_in_usd['CAD=']
close_prices_in_usd['JPY='] = 1.0 / close_prices_in_usd['JPY=']

gbp = close_prices_in_usd['GBP=']
close_prices_in_usd['RDSa.L'] *= gbp / 100.0
close_prices_in_usd['HSBA.L'] *= gbp / 100.0
close_prices_in_usd['BATS.L'] *= gbp / 100.0
close_prices_in_usd['AZN.L']  *= gbp / 100.0

cad = close_prices_in_usd['CAD=']
close_prices_in_usd['RY.TO']  *= cad
close_prices_in_usd['CNR.TO'] *= cad
close_prices_in_usd['SU.TO']  *= cad
close_prices_in_usd['ENB.TO'] *= cad
close_prices_in_usd['CNQ.TO'] *= cad

jpy = close_prices_in_usd['JPY=']
close_prices_in_usd['9983.T'] *= jpy
close_prices_in_usd['7751.T'] *= jpy
close_prices_in_usd['9022.T'] *= jpy
close_prices_in_usd['6902.T'] *= jpy
close_prices_in_usd['6367.T'] *= jpy

eur = close_prices_in_usd['EUR=']
close_prices_in_usd['LVMH.PA'] *= eur
close_prices_in_usd['TTEF.PA'] *= eur
close_prices_in_usd['SAPG.DE'] *= eur
close_prices_in_usd['OREP.PA'] *= eur

log_returns_in_usd = close_prices_in_usd.apply(np.log).diff()
# equivalently: close_prices_in_usd.pct_change().apply(np.log1p)
exp_in_usd1 = log_returns_in_usd.mean()
cov_in_usd1 = log_returns_in_usd.cov()

#### METHOD 2 - SMARTER WAY

log_returns = close_prices.apply(np.log).diff()
# equivalently: close_prices.pct_change().apply(np.log1p)
exp = log_returns.mean()
cov = log_returns.cov()

reciprocate_fx = pd.DataFrame(np.identity(len(cov.columns)), columns=instruments.index, index=instruments.index)
reciprocate_fx.loc['CAD=','CAD='] = -1
reciprocate_fx.loc['JPY=','JPY='] = -1

chain_fx = pd.DataFrame(np.identity(len(cov.columns)), columns=instruments.index, index=instruments.index)
stocks_gbp = ['RDSa.L', 'HSBA.L', 'BATS.L', 'AZN.L']
chain_fx.loc['GBP=', stocks_gbp] = 1.0
stocks_cad = ['RY.TO', 'CNR.TO', 'SU.TO', 'ENB.TO', 'CNQ.TO']
chain_fx.loc['CAD=', stocks_cad] = 1.0
stocks_jpy = ['9983.T', '7751.T', '9022.T', '6902.T', '6367.T']
chain_fx.loc['JPY=', stocks_jpy] = 1.0
stocks_eur = ['LVMH.PA', 'TTEF.PA', 'SAPG.DE', 'OREP.PA']
chain_fx.loc['EUR=', stocks_eur] = 1.0

fx = reciprocate_fx @ chain_fx
exp_in_usd2 = exp @ fx
cov_in_usd2 = fx.transpose() @ cov @ fx

#### MARKET PORTFOLIO

## NAIVE

expectation_of_market_portfolio_naive = exp @ market_portfolio_weights
variance_of_market_portfolio_naive = market_portfolio_weights.transpose() @ cov @ market_portfolio_weights

## BRUTE FORCE 1

market_portfolio_returns = log_returns_in_usd @ market_portfolio_weights
expectation_of_market_portfolio0 = market_portfolio_returns.mean()
variance_of_market_portfolio0 = market_portfolio_returns.var()

## BRUTE FORCE 2

expectation_of_market_portfolio1 = exp_in_usd1 @ market_portfolio_weights
variance_of_market_portfolio1 = market_portfolio_weights.transpose() @ cov_in_usd1 @ market_portfolio_weights

## SMARTER WAY

expectation_of_market_portfolio2 = exp_in_usd2 @ market_portfolio_weights
variance_of_market_portfolio2 = market_portfolio_weights.transpose() @ cov_in_usd2 @ market_portfolio_weights

## MODIFIED WEIGHTS

market_portfolio_modified_weights = fx @ market_portfolio_weights
expectation_of_market_portfolio3 = exp @ market_portfolio_modified_weights
variance_of_market_portfolio3 = market_portfolio_modified_weights.transpose() @ cov @ market_portfolio_modified_weights

representative_investor_risk_aversion = expectation_of_market_portfolio3 / variance_of_market_portfolio3

asset_drifts_usd = representative_investor_risk_aversion * cov_in_usd2 @ market_portfolio_weights
asset_drifts_usd_annualised = asset_drifts_usd * 260
