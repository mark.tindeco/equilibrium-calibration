import pandas as pd
import numpy as np

close_prices = pd.read_csv("close prices.csv", index_col=0, parse_dates=True, dayfirst=True).dropna()

# reciprocate CAD and JPY exchange rates to denominate in USD
close_prices['CAD='] = 1.0 / close_prices['CAD=']
close_prices['JPY='] = 1.0 / close_prices['JPY=']

instruments = pd.read_csv("instruments.csv", index_col=0)
market_portfolio_weights = instruments['Weight']

#### METHOD 1 - BRUTE FORCE

close_prices_in_usd = close_prices.copy()

gbp = close_prices_in_usd['GBP=']
close_prices_in_usd['RDSa.L'] *= gbp / 100.0
close_prices_in_usd['HSBA.L'] *= gbp / 100.0
close_prices_in_usd['BATS.L'] *= gbp / 100.0
close_prices_in_usd['AZN.L']  *= gbp / 100.0

cad = close_prices_in_usd['CAD=']
close_prices_in_usd['RY.TO']  *= cad
close_prices_in_usd['CNR.TO'] *= cad
close_prices_in_usd['SU.TO']  *= cad
close_prices_in_usd['ENB.TO'] *= cad
close_prices_in_usd['CNQ.TO'] *= cad

jpy = close_prices_in_usd['JPY=']
close_prices_in_usd['9983.T'] *= jpy
close_prices_in_usd['7751.T'] *= jpy
close_prices_in_usd['9022.T'] *= jpy
close_prices_in_usd['6902.T'] *= jpy
close_prices_in_usd['6367.T'] *= jpy

eur = close_prices_in_usd['EUR=']
close_prices_in_usd['LVMH.PA'] *= eur
close_prices_in_usd['TTEF.PA'] *= eur
close_prices_in_usd['SAPG.DE'] *= eur
close_prices_in_usd['OREP.PA'] *= eur

log_returns_in_usd = close_prices_in_usd.apply(np.log).diff()
# equivalently .pct_change().apply(np.log1p)

mean_log_returns_in_usd1 = log_returns_in_usd.mean()
cov_log_returns_in_usd1 = log_returns_in_usd.cov()

#### METHOD 2 - SMARTER WAY

log_returns = close_prices.apply(np.log).diff()
# equivalently .pct_change().apply(np.log1p)
mean_log_returns = log_returns.mean()
cov_log_returns = log_returns.cov()

fx = pd.DataFrame(np.identity(len(cov_log_returns.columns)), columns=instruments.index, index=instruments.index)
stocks_gbp = ['RDSa.L', 'HSBA.L', 'BATS.L', 'AZN.L']
stocks_cad = ['RY.TO', 'CNR.TO', 'SU.TO', 'ENB.TO', 'CNQ.TO']
stocks_jpy = ['9983.T', '7751.T', '9022.T', '6902.T', '6367.T']
stocks_eur = ['LVMH.PA', 'TTEF.PA', 'SAPG.DE', 'OREP.PA']
fx.loc['GBP=', stocks_gbp] = 1.0
fx.loc['CAD=', stocks_cad] = 1.0
fx.loc['JPY=', stocks_jpy] = 1.0
fx.loc['EUR=', stocks_eur] = 1.0

#fx_inv = pd.DataFrame(np.around(np.linalg.pinv(fx.values), 5), fx.columns, fx.index)

mean_log_returns_in_usd2 = mean_log_returns @ fx
cov_log_returns_in_usd2 = fx.transpose() @ cov_log_returns @ fx

#### MARKET PORTFOLIO

## NAIVE

expectation_of_market_portfolio_naive = mean_log_returns @ market_portfolio_weights
variance_of_market_portfolio_naive = market_portfolio_weights.transpose() @ cov_log_returns @ market_portfolio_weights

## BRUTE FORCE 1

market_portfolio_returns = log_returns_in_usd @ market_portfolio_weights
expectation_of_market_portfolio0 = market_portfolio_returns.mean()
variance_of_market_portfolio0 = market_portfolio_returns.var()

## BRUTE FORCE 2

expectation_of_market_portfolio1 = mean_log_returns_in_usd1 @ market_portfolio_weights
variance_of_market_portfolio1 = market_portfolio_weights.transpose() @ cov_log_returns_in_usd1 @ market_portfolio_weights

## SMARTER WAY

expectation_of_market_portfolio2 = mean_log_returns_in_usd2 @ market_portfolio_weights
variance_of_market_portfolio2 = market_portfolio_weights.transpose() @ cov_log_returns_in_usd2 @ market_portfolio_weights

## MODIFIED WEIGHTS

market_portfolio_modified_weights = fx @ market_portfolio_weights
expectation_of_market_portfolio3 = mean_log_returns @ market_portfolio_modified_weights
variance_of_market_portfolio3 = market_portfolio_modified_weights.transpose() @ cov_log_returns @ market_portfolio_modified_weights

## EQUILIBRIUM CALIBRATION

representative_investor_risk_aversion = expectation_of_market_portfolio3 / variance_of_market_portfolio3

asset_drifts_usd = representative_investor_risk_aversion * cov_log_returns_in_usd2 @ market_portfolio_weights
asset_drifts_usd_annualised = asset_drifts_usd * 260

asset_drifts_local = asset_drifts_usd.copy()
asset_drifts_local[stocks_gbp] -= asset_drifts_local['GBP='] + cov_log_returns.loc['GBP=', stocks_gbp]
asset_drifts_local[stocks_cad] -= asset_drifts_local['CAD='] + cov_log_returns.loc['CAD=', stocks_cad]
asset_drifts_local[stocks_jpy] -= asset_drifts_local['JPY='] + cov_log_returns.loc['JPY=', stocks_jpy]
asset_drifts_local[stocks_eur] -= asset_drifts_local['EUR='] + cov_log_returns.loc['EUR=', stocks_eur]
asset_drifts_local_annualised = asset_drifts_local * 260

asset_drifts_local_annualised_percentage = pd.Series(["{0:.4f}%".format(val * 100) for val in asset_drifts_local_annualised], index = asset_drifts_local_annualised.index)

print("Expected log returns of market portfolio:", expectation_of_market_portfolio0)
print("Expected log returns of market portfolio:", asset_drifts_usd @ market_portfolio_weights)

cov_log_returns.to_pickle('cov_log_returns_local.pkl')
asset_drifts_local.to_pickle('mean_log_returns_local.pkl')
market_portfolio_weights.to_pickle('weights.pkl')

cov_log_returns_in_usd2.to_pickle('cov_log_returns_usd.pkl')
asset_drifts_usd.to_pickle('mean_log_returns_usd.pkl')
