import pandas as pd
import matplotlib.pyplot as plt

close_prices = pd.read_csv("close prices.csv", index_col=0, parse_dates=True, dayfirst=True)
instruments = pd.read_csv("instruments.csv", index_col=0)

#symbol = 'EUR=' # 'AAPL.O' # 'EUR='
#name = instruments.loc[symbol, 'Name']

symbols = instruments.index

for symbol, row in instruments.iterrows():
    plt.figure()
    name = row['Name']
    close_prices[symbol].plot(title=name)
