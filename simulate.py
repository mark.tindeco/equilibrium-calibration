import pandas as pd
import numpy as np 
import numpy.random as nr
import scipy.optimize as optimize

cov_log_returns_local = pd.read_pickle('cov_log_returns_local.pkl')
mean_log_returns_local = pd.read_pickle('mean_log_returns_local.pkl')

trials = np.arange(10_000)
time_steps = np.arange(266) # 266
representative_investor_risk_aversion = 2.1335579271755662

size = trials.size * time_steps.size

#cholesky = pd.DataFrame(np.linalg.cholesky(cov), columns=cov.index, index=cov.index)
#check = cholesky @ cholesky.transpose() / cov

nr.seed(0)
midx = pd.MultiIndex.from_product([trials, time_steps], names=['trial', 'time_step'])
asset_total_returns = pd.DataFrame(np.exp(nr.multivariate_normal(mean_log_returns_local - 0.5 * np.diagonal(cov_log_returns_local), cov_log_returns_local, size=size)), index=midx, columns=cov_log_returns_local.index)

drifts_sampled = (asset_total_returns - 1.0).mean()
drifts_expected = mean_log_returns_local.apply(np.exp) - 1.0
drifts_check = drifts_sampled / drifts_expected

cov_sampled = asset_total_returns.cov()
cov_expected = (cov_log_returns_local.apply(np.exp) - 1.0) * np.outer(1.0 + drifts_expected, 1.0 + drifts_expected)
cov_check = cov_sampled / cov_expected

# stocks_gbp = ['RDSa.L', 'HSBA.L', 'BATS.L', 'AZN.L']
# stocks_cad = ['RY.TO', 'CNR.TO', 'SU.TO', 'ENB.TO', 'CNQ.TO']
# stocks_jpy = ['9983.T', '7751.T', '9022.T', '6902.T', '6367.T']
# stocks_eur = ['LVMH.PA', 'TTEF.PA', 'SAPG.DE', 'OREP.PA']

fx_simulated_returns = asset_total_returns['GBP=']
asset_total_returns['RDSa.L'] *= fx_simulated_returns
asset_total_returns['HSBA.L'] *= fx_simulated_returns
asset_total_returns['BATS.L'] *= fx_simulated_returns
asset_total_returns['AZN.L']  *= fx_simulated_returns

fx_simulated_returns = asset_total_returns['CAD=']
asset_total_returns['RY.TO']  *= fx_simulated_returns
asset_total_returns['CNR.TO'] *= fx_simulated_returns
asset_total_returns['SU.TO']  *= fx_simulated_returns
asset_total_returns['ENB.TO'] *= fx_simulated_returns
asset_total_returns['CNQ.TO'] *= fx_simulated_returns

fx_simulated_returns = asset_total_returns['JPY=']
asset_total_returns['9983.T'] *= fx_simulated_returns
asset_total_returns['7751.T'] *= fx_simulated_returns
asset_total_returns['9022.T'] *= fx_simulated_returns
asset_total_returns['6902.T'] *= fx_simulated_returns
asset_total_returns['6367.T'] *= fx_simulated_returns

fx_simulated_returns = asset_total_returns['EUR=']
asset_total_returns['LVMH.PA'] *= fx_simulated_returns
asset_total_returns['TTEF.PA'] *= fx_simulated_returns
asset_total_returns['SAPG.DE'] *= fx_simulated_returns
asset_total_returns['OREP.PA'] *= fx_simulated_returns

initial_weights = pd.read_pickle('weights.pkl')

#portfolio_total_returns = asset_total_returns @ weights
#portfolio_total_returns_over_horizon = portfolio_total_returns.apply(np.log).groupby('trial').sum().apply(np.exp)

def negative_expected_utility(weights, risk_aversion):
    #print(weights)
    one_minus_risk_aversion = 1 - risk_aversion
    portfolio_expected_utility = (one_minus_risk_aversion * (asset_total_returns @ weights + (1 - weights.sum())).apply(np.log).groupby('trial').sum()).apply(np.exp).mean() / one_minus_risk_aversion
    return -portfolio_expected_utility

def expected_returns(weights):
    return -negative_expected_utility(initial_weights, 0) - 1

def jacobian(weights, risk_aversion):
    one_minus_risk_aversion = 1 - risk_aversion
    portfolio_total_returns = asset_total_returns @ weights + (1 - weights.sum())
    return -(asset_total_returns - 1).div(portfolio_total_returns, axis=0).groupby('trial').sum().multiply((one_minus_risk_aversion * portfolio_total_returns.apply(np.log).groupby('trial').sum()).apply(np.exp), axis=0).mean()

portfolio_expected_return_simulated = expected_returns(initial_weights)
portfolio_expected_utility_simulated = -negative_expected_utility(initial_weights, representative_investor_risk_aversion)

#jacobean_simulated_val = jacobian(initial_weights, representative_investor_risk_aversion)

result = optimize.minimize(negative_expected_utility, initial_weights.values, args=(representative_investor_risk_aversion,), jac=jacobian)
if result.success:
    optimal_weights = result.x
    print(optimal_weights)
else:
    raise ValueError(result.message)
